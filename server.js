#!/usr/bin/env node
'use strict'

const express = require('express')
const morgan = require('morgan')
const passport = require('passport')
const path = require('path')
const compression = require('compression')
const session = require('express-session')
const lastMile = require('connect-lastmile')
const bodyParser = require('body-parser')
const fs = require('fs')
const openvpn = require('./src/openvpn')
const CloudronStrategy = require('passport-cloudron')
const LokiStore = require('connect-loki')(session)

const app = express()
const router = new express.Router()

const urlEncodedParser = bodyParser.urlencoded({ extended: true })

const cleanProfile = profile => ({
  id: profile.id,
  username: profile.username,
  email: profile.email,
  alternateEmail: profile.alternateEmail,
  admin: profile.admin,
  displayName: profile.displayName
})

passport.serializeUser((user, done) => { done(null, JSON.stringify(user)) })

passport.deserializeUser((str, done) => {
  try {
    done(null, cleanProfile(JSON.parse(str)))
  } catch (e) {
    done(e)
  }
})

const onCloudron = Boolean(process.env.CLOUDRON)

if (onCloudron) {
  passport.use(new CloudronStrategy(
    {callbackURL: process.env.APP_ORIGIN + '/login'},
    (token, tokenSecret, profile, done) => done(null, cleanProfile(profile))
  ))
  app.set('trust proxy', 1)
}

const isAuthenticated = (req, res, next) => (req.isAuthenticated() || !onCloudron) // When not on Cloudron, no auth
  ? next()
  : res.status(401).send({})

const baseDir = '/app/data'

app.use('/api/healthcheck', (req, res) => {
  if (openvpn.isRunning()) res.status(200).send()
  else res.status(500).send()
})

app.use(morgan('dev'))
app.use(compression())
app.use(session({
  secret: fs.readFileSync(path.resolve(__dirname, `${baseDir}/session.secret`), 'utf8'),
  store: new LokiStore({
    path: path.resolve(__dirname, `${baseDir}/session.db`),
    logErrors: true
  }),
  resave: false,
  saveUninitialized: false,
  name: 'openvpn.sid',
  cookie: {
    path: '/',
    httpOnly: true,
    secure: onCloudron,
    maxAge: /* 1 week: */ 7 /* d */ * 24 /* h */ * 60 /* min */ * 60 /* s */ * 1000 /* ms */
  }
}))
app.use(passport.initialize())
app.use(passport.session())
app.use('/login', passport.authenticate('cloudron'), (req, res) => res.redirect('/'))
app.use('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})
router.post('/api/onConnect/', urlEncodedParser, openvpn.onClientConnect)
router.post('/api/onDisconnect/', urlEncodedParser, openvpn.onClientDisconnect)
router.get('/api/list/', isAuthenticated, openvpn.list)
router.put('/api/key/*', isAuthenticated, openvpn.createKey)
router.get('/api/key/*', isAuthenticated, openvpn.getKey)
router.delete('/api/key/*', isAuthenticated, openvpn.revokeKey)
app.use(router)
app.use('/', express.static(path.resolve(__dirname, 'app')))
app.use(lastMile())

const server = app.listen(3000, () => {
  const {host, port} = server.address()

  if (!onCloudron) console.warn('NOT running on Cloudron! Auth is disabled')
  console.log(`OpenVPN web-config interface listening at http://${host}:${port}`)
})
