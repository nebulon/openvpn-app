# OpenVPN

This is an OpenVPN server, packaged with a very simple key manager

## Usage

The key management interface is available under the `/` location. Each Cloudron-authenticated user can create and download keys for themself.
