'use strict'

const path = require('path')
const assert = require('assert')
const childProcess = require('child_process')
const fs = require('fs')
const {HttpError} = require('connect-lastmile')
const Archiver = require('archiver')

const baseDir = '/app/code/easyrsa'
const keyDir = '/app/data/keys'

const env = {
  EASY_RSA: baseDir,
  OPENSSL: 'openssl',
  PKCS11TOOL: 'pkcs11-tool',
  GREP: 'grep',
  KEY_DIR: keyDir,
  PKCS11_MODULE_PATH: 'dummy',
  PKCS11_PIN: 'dummy',
  KEY_SIZE: 2048,
  CA_EXPIRE: 3650,
  KEY_EXPIRE: 3650,
  KEY_COUNTRY: 'US',
  KEY_PROVINCE: 'CA',
  KEY_CITY: 'SanFrancisco',
  KEY_ORG: 'Cloudron',
  KEY_EMAIL: 'support@cloudron.io',
  KEY_OU: 'Cloudron',
  KEY_NAME: 'EasyRSA',
  KEY_CONFIG: '/app/code/easyrsa/openssl-1.0.0.cnf'
}

// Provide either keyName, or the other 4 args
const clientConfFile = ({ca, cert, key, tlsAuth, keyName}) => `# Client
client
tls-client
dev tun
proto tcp-client
remote ${process.env.APP_DOMAIN} ${process.env.VPN_TCP_PORT}
resolv-retry infinite
cipher AES-256-CBC
auth SHA256
script-security 2
keepalive 10 120
ns-cert-type server

# Keys
${ca
  ? '<ca>\n' + ca + '\n</ca>'
  : 'ca ca.crt'
  }
${cert
  ? '<cert>\n' + cert + '\n</cert>'
  : 'cert ' + keyName + '.crt'
  }
${cert
  ? '<key>\n' + key + '\n</key>'
  : 'key ' + keyName + '.key'
  }
${tlsAuth
  ? 'key-direction 1\n<tls-auth>\n' + tlsAuth + '\n</tls-auth>'
  : 'tls-auth ta.key 1'
  }

# Security
nobind
persist-key
persist-tun
verb 3
`

const connectedClients = {}

const exec = ({tag, file, args, wantedCode = 0}) => new Promise((resolve, reject) => {
  assert.strictEqual(typeof tag, 'string')
  assert.strictEqual(typeof file, 'string')
  assert(Array.isArray(args))

  console.log(`${tag} execFile: ${file} ${args.join(' ')}`)

  const cp = childProcess.spawn(file, args, {env})
  cp.stdout.on('data', data => console.log(`${tag} (stdout): ${data.toString('utf8')}`))

  let errorData = ''
  cp.stderr.on('data', data => {
    errorData += data.toString('utf8') // print only on newline
    while (errorData.includes('\n')) {
      const parts = errorData.split('\n')
      console.log(`${tag} (stderr): ${parts.shift()}`)
      errorData = parts.join('\n')
    }
  })

  cp.on('exit', (code, signal) => {
    if (errorData) console.log(`${tag} (stderr): ${errorData}`)
    if (code || signal) console.log(`${tag} code: ${code}, signal: ${signal}`)
    if (code === wantedCode) return resolve()

    const e = new Error(`${tag} exited with error ${code} signal ${signal}`)
    e.code = code
    e.signal = signal
    reject(e)
  })

  cp.on('error', error => {
    console.log(`${tag} code: ${error.code}, signal: ${error.signal}`)
    reject(error)
  })
})

const cleanUserName = userName => userName.toLowerCase().replace(/[^A-Za-z0-9.]+/g, '')

const cleanDeviceName = deviceName => deviceName.replace(/[^A-Za-z0-9\-_]+/g, '')

const promisify = (func) => (...args) => new Promise((resolve, reject) =>
  func(...args, (err, res) => err ? reject(err) : resolve(res))
)

const readdir = promisify(fs.readdir)
const stat = promisify(fs.stat)
const rm = promisify(fs.unlink)
const readFile = promisify(fs.readFile)

const ADMIN_TOKEN = fs.readFileSync('/run/admin-token', 'utf8')

const listUserKeys = (user) => readdir(keyDir)
  .then(files => files.filter(file => file.startsWith(cleanUserName(user) + ':') && file.endsWith('.key')))
  .then(files => Promise.all(files.map(file => stat(path.join(keyDir, file))))
    .then(stats => files.map((fileName, i) => {
      const deviceName = fileName.substring(cleanUserName(user).length + 1, fileName.length).replace(/\.key/, '')
      return {
        name: deviceName,
        created: stats[i].birthtime,
        connected: (connectedClients[user] && connectedClients[user][deviceName]) || false // object if exists, false if undefined or null
      }
    }))
  )

const list = (req, res, next) => {
  const user = req.user && req.user.username ? req.user.username : 'anonymous'
  if (!user) return next(new HttpError(401, 'Unidentified'))
  listUserKeys(user)
    .then(list => res.status(222).send({
      entries: list,
      admin: req.user.admin
        ? {connectedClients}
        : false
    }))
    .catch(error => next(new HttpError(500, error)))
}

const createKey = (req, res, next) => {
  const user = req.user && req.user.username ? req.user.username : 'anonymous'
  const deviceName = decodeURIComponent(req.params[0])

  if (!user) return next(new HttpError(401, 'Unidentified'))
  if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'))

  listUserKeys(user)
    .then(list => {
      if (list.map(e => e.name).includes(deviceName)) throw new Error('Device already exists')
      return exec({
        tag: 'createUserKey',
        file: path.join(baseDir, 'pkitool'),
        args: [`${cleanUserName(user)}:${deviceName}`]
      })
        .then(() => res.status(201).send({created: deviceName}))
    })
    .catch(error => next(new HttpError(500, error)))
}

const getKey = (req, res, next) => {
  const user = req.user && req.user.username ? cleanUserName(req.user.username) : 'anonymous'
  const deviceName = decodeURIComponent(req.params[0])
  const format = req.query['format'] || 'conf'
  const zip = !(req.query['zip'] === 'false') // default to true

  if (!user) return next(new HttpError(401, 'Unidentified'))
  if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'))

  let internalPathPrefix // path inside the zip file, if zipped
  let configExt // extension of the config file

  if (format === 'conf') {
    internalPathPrefix = ''
    configExt = 'conf'
  } else if (format === 'ovpn') {
    internalPathPrefix = ''
    configExt = 'ovpn'
  } else if (format === 'tblk') {
    if (!zip) return next(new HttpError(409, 'Invalid format: cannot disable zip for tblk'))
    internalPathPrefix = `${process.env.APP_DOMAIN}-${deviceName}.tblk/Contents/Resources/`
    configExt = 'ovpn'
  } else {
    return next(new HttpError(409, 'Invalid format'))
  }

  listUserKeys(user)
    .then(list => {
      if (!list.map(e => e.name).includes(deviceName)) return next(new HttpError(404, 'Not Found'))
      const certFile = `${user}:${deviceName}.crt`
      const keyFile = `${user}:${deviceName}.key`

      if (zip) {
        res.header('Content-Type', 'application/zip')
        res.header('Content-Disposition', `attachment; filename="${process.env.APP_DOMAIN}-${deviceName}-${format}.zip"`)

        const archive = new Archiver('zip')
        archive.on('warning', (err) => console.error('ZIP WARNING:', err))
        archive.on('error', (err) => {
          console.error('ZIP ERROR:', err)
          res.end()
        })
        archive.pipe(res)

        archive.file(path.join(keyDir, 'ca.crt'), {name: internalPathPrefix + 'ca.crt'})
        archive.file(path.join(keyDir, certFile), {name: internalPathPrefix + certFile})
        archive.file(path.join(keyDir, keyFile), {name: internalPathPrefix + keyFile})
        archive.file(path.join(keyDir, 'ta.key'), {name: internalPathPrefix + 'ta.key'})
        archive.append(
          clientConfFile({keyName: `${user}:${deviceName}`}),
          {name: internalPathPrefix + 'config.' + configExt}
        )
        archive.finalize()
      } else {
        return Promise.all([
          readFile(path.join(keyDir, 'ca.crt'), 'utf8'),
          readFile(path.join(keyDir, certFile), 'utf8'),
          readFile(path.join(keyDir, keyFile), 'utf8'),
          readFile(path.join(keyDir, 'ta.key'), 'utf8')
        ])
          .then(([ca, cert, key, tlsAuth]) => {
            res.header('Content-Type', 'text/plain')
            res.header('Content-Disposition', `attachment; filename="${process.env.APP_DOMAIN}-${deviceName}.${configExt}"`)
            res.send(clientConfFile({
              ca,
              cert,
              key,
              tlsAuth
            }))
          })
      }
    })
    .catch(error => next(new HttpError(500, error)))
}

const revokeKey = (req, res, next) => {
  const user = req.user && req.user.username ? cleanUserName(req.user.username) : 'anonymous'
  const deviceName = decodeURIComponent(req.params[0])

  if (!user) return next(new HttpError(401, 'Unidentified'))
  if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'))

  listUserKeys(user)
    .then(list => {
      if (!list.map(e => e.name).includes(deviceName)) return next(new HttpError(404, 'Not Found'))

      return exec({
        tag: 'revokeUserKey',
        file: path.join(baseDir, 'revoke-full'),
        args: [`${cleanUserName(user)}:${deviceName}`],
        wantedCode: 2
      })
        .then(() => rm(path.join(keyDir, `${cleanUserName(user)}:${deviceName}.key`)))
        .then(() => res.status(200).send({revoked: deviceName}))
    })
    .catch(error => next(new HttpError(500, error)))
}

const onClientConnect = (req, res, next) => {
  const cn = req.body['cn']
  const ip = req.body['ip']
  const vpnIp = req.body['vpnIp']
  const token = req.body['token']

  if (token !== ADMIN_TOKEN) return next(new HttpError(401, 'Unauthorized'))
  if (!cn || !ip || !vpnIp) return next(new HttpError(409, 'Invalid Request'))

  const match = /^([A-Za-z0-9.]+):([A-Za-z0-9\-_]+)$/.exec(cn)
  if (!match) return next(new HttpError(409, 'Invalid Request'))
  const [, user, deviceName] = match

  if (!connectedClients[user]) connectedClients[user] = {}

  connectedClients[user][deviceName] = {ip, vpnIp}
  return res.status(200).send({connected: {user, deviceName, ip, vpnIp}})
}

const onClientDisconnect = (req, res, next) => {
  const cn = req.body['cn']
  const token = req.body['token']

  if (token !== ADMIN_TOKEN) return next(new HttpError(401, 'Unauthorized'))
  if (!cn) return next(new HttpError(409, 'Invalid Request'))

  const match = /^([A-Za-z0-9.]+):([A-Za-z0-9\-_]+)$/.exec(cn)
  if (!match) return next(new HttpError(409, 'Invalid Request'))
  const [, user, deviceName] = match

  if (connectedClients[user] && connectedClients[user][deviceName]) {
    delete connectedClients[user][deviceName]
  }

  return res.status(200).send({disconnected: {user, deviceName}})
}

const isRunning = () => {
  try {
    return childProcess.execSync('supervisorctl status openvpn')
      .toString()
      .includes('RUNNING')
  } catch (err) {
    return false
  }
}

module.exports = {
  list,
  createKey,
  getKey,
  revokeKey,
  isRunning,
  onClientConnect,
  onClientDisconnect
}
