#!/bin/bash

set -eu

export NODE_ENV=production

# Creating a secret for passport's sessions
if [ ! -f /app/data/session.secret ]; then
    dd if=/dev/urandom bs=256 count=1 | base64 > /app/data/session.secret
fi

# Generate random management token for admin api
dd if=/dev/urandom bs=256 count=1 | base64 > /run/admin-token

export EASY_RSA="/app/code/easyrsa/"
export OPENSSL="openssl"
export PKCS11TOOL="pkcs11-tool"
export GREP="grep"
export KEY_DIR="/app/data/keys/"
export PKCS11_MODULE_PATH="dummy"
export PKCS11_PIN="dummy"
export KEY_SIZE=2048
export CA_EXPIRE=3650
export KEY_EXPIRE=3650
export KEY_COUNTRY="US"
export KEY_PROVINCE="CA"
export KEY_CITY="SanFrancisco"
export KEY_ORG="Cloudron"
export KEY_EMAIL="support@cloudron.io"
export KEY_OU="Cloudron"
export KEY_NAME="EasyRSA"
export KEY_CONFIG="/app/code/easyrsa/openssl-1.0.0.cnf"

# The first time this is run, initialize OpenVPN keys
if [ ! -d /app/data/keys ]; then
    /app/code/easyrsa/clean-all
    /app/code/easyrsa/pkitool --initca
    openvpn --genkey --secret /app/data/keys/ta.key
    /app/code/easyrsa/build-dh
    /app/code/easyrsa/pkitool --server cloudron

    # Generating empty CRL file
    KEY_ALTNAMES="" KEY_CN="" ${OPENSSL} ca -gencrl -out /app/data/keys/crl.pem -config "$KEY_CONFIG"
fi

# Writing OpenVPN config
./openvpn-conf.sh > /run/openvpn.conf

# Add iptables rules for NATing VPN traffic
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

# Fix permissions
chown -R cloudron:cloudron /app/data /tmp /run

echo "Starting server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OpenVPN
