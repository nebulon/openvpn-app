# OpenVPN

This is an OpenVPN server, packaged with a very simple key manager

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.cloudron.openvpn)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.cloudron.openvpn
```

## Building

### Cloudron
The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/cloudron/openvpn-app.git
cd openvpn-app
cloudron build
cloudron install
```

## Usage

The key management interface is available under the `/` location. Each Cloudron-authenticated user can create and download keys for themself.

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the docs are ok.

```
cd openvpn-app

npm install
CLOUDRON_USERNAME=<cloudron username> CLOUDRON_PASSWORD=<cloudron password> npm test
```

