#!/usr/bin/env bash

curl 'http://127.0.0.1:3000/api/onConnect/' -X POST \
    --data-urlencode "cn=${common_name}" \
    --data-urlencode "vpnIp=${ifconfig_pool_remote_ip}" \
    --data-urlencode "ip=${trusted_ip}" \
    --data-urlencode "token@/run/admin-token"
