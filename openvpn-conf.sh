#!/bin/bash

echo "# Server TCP/${VPN_TCP_PORT:?}
mode server
tls-server
proto tcp
port ${VPN_TCP_PORT:?}
dev tun
dev-node /app/code/net-tun
# Keys and certificates
ca /app/data/keys/ca.crt
cert /app/data/keys/cloudron.crt
key /app/data/keys/cloudron.key
dh /app/data/keys/dh2048.pem
tls-auth /app/data/keys/ta.key 0
crl-verify /app/data/keys/crl.pem
cipher AES-256-CBC
auth SHA256
# Network
server 10.8.0.0 255.255.255.0
push \"redirect-gateway def1 bypass-dhcp\"
push \"dhcp-option DNS 8.8.4.4\"
push \"dhcp-option DNS 8.8.8.8\"
client-to-client
keepalive 10 120
# Security
user cloudron
group cloudron
persist-key
persist-tun
# Log
verb 3
mute 20
status /run/openvpn-status.log
# Hooks to update server status
script-security 2
client-connect /app/code/openvpn-on-client-connect.sh
client-disconnect /app/code/openvpn-on-client-disconnect.sh
"
