FROM cloudron/base:0.10.0
MAINTAINER Mehdi Kouhen <arantes555@gmail.com>

ENV PATH /usr/local/node-6.9.5/bin:$PATH

## Adding OpenVPN repos
RUN wget -O - https://swupdate.openvpn.net/repos/repo-public.gpg|apt-key add -
RUN echo "deb http://build.openvpn.net/debian/openvpn/stable xenial main" > /etc/apt/sources.list.d/openvpn-aptrepo.list

## Installing OpenVPN, key-management tool, and iptables
RUN apt-get update -y
RUN apt-get install -y openvpn=2.4.3-* easy-rsa iptables

RUN mkdir -p /app/code
WORKDIR /app/code

## Creating easyRSA dir & configuring
RUN make-cadir /app/code/easyrsa
RUN chmod +rx /app/code/easyrsa
RUN sed -e 's,^RANDFILE\s*= \$ENV::HOME/\.rnd$,RANDFILE                = /tmp/.rnd,' -i /app/code/easyrsa/openssl-1.0.0.cnf
# Workaround quantum permissions bug
RUN chown -R cloudron:cloudron /app/code/easyrsa

## Installing web-admin interface & packaging scripts
ADD package.json /app/code/
RUN npm install --production
ADD src /app/code/src
ADD app /app/code/app
# Somehow postinstall is not run automatically when building docker image
RUN npm run postinstall

ADD start.sh server.js openvpn-conf.sh openvpn-on-client-connect.sh openvpn-on-client-disconnect.sh /app/code/
RUN chmod +x start.sh openvpn-conf.sh openvpn-on-client-connect.sh openvpn-on-client-disconnect.sh

RUN mkdir -p /app/data

## Setting up TUN device
RUN mknod /app/code/net-tun c 10 200

## Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

EXPOSE 8000

CMD [ "/app/code/start.sh" ]
