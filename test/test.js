#!/usr/bin/env node

/* global describe, before, after, xit, it */
'use strict'

const execSync = require('child_process').execSync
const expect = require('expect.js')
const net = require('net')
const path = require('path')

const webdriver = require('selenium-webdriver')

const by = webdriver.By
const until = webdriver.until

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

if (!process.env.CLOUDRON_USERNAME || !process.env.CLOUDRON_PASSWORD) {
  console.log('CLOUDRON_USERNAME and CLOUDRON_PASSWORD env vars need to be set')
  process.exit(1)
}

describe('Application life cycle test', function () {
  this.timeout(0)

  const chrome = require('selenium-webdriver/chrome')
  let server
  const browser = new chrome.Driver()
  const username = process.env.CLOUDRON_USERNAME
  const password = process.env.CLOUDRON_PASSWORD

  before(function () {
    const seleniumJar = require('selenium-server-standalone-jar')
    const SeleniumServer = require('selenium-webdriver/remote').SeleniumServer
    server = new SeleniumServer(seleniumJar.path, {port: 4444})
    server.start()
  })

  after(function () {
    browser.quit()
    server.stop()
  })

  const LOCATION = 'test'
  const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000
  let app

  function waitForElement (elem) {
    return browser.wait(until.elementLocated(elem), TEST_TIMEOUT)
      .then(() => browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT))
  }

  function getAppInfo () {
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0]
    expect(app).to.be.an('object')
  }

  function login () {
    browser.manage().deleteAllCookies()
    browser.get('https://' + app.fqdn)

    return waitForElement(by.id('loginButton'))
      .then(() => browser.findElement(by.id('loginButton')).click())
      .then(() => browser.wait(() => browser.getCurrentUrl().then(url => url.indexOf('/api/v1/session/login') !== -1), TEST_TIMEOUT))
      .then(() => browser.findElement(by.id('inputUsername')).sendKeys(username))
      .then(() => browser.findElement(by.id('inputPassword')).sendKeys(password))
      .then(() => browser.findElement(by.tagName('form')).submit())
      .then(() => browser.wait(until.elementLocated(by.xpath('//a[text()="Logout"]')), TEST_TIMEOUT))
  }

  // after first login, we have an oauth session already
  function autoLogin () {
    browser.manage().deleteAllCookies()
    browser.get('https://' + app.fqdn)

    return waitForElement(by.id('loginButton'))
      .then(() => browser.findElement(by.id('loginButton')).click())
      .then(() => waitForElement(by.id('logoutButton')))
  }

  function logout () {
    browser.get('https://' + app.fqdn)

    return browser.findElement(by.id('logoutButton')).click()
      .then(() => waitForElement(by.id('loginButton')))
  }

  function createDevice (name) {
    return browser.findElement(by.xpath('//button[text()="New Device"]')).click()
      .then(() => waitForElement(by.xpath('//input[@id="createKeyData"]')))
      .then(() => browser.findElement(by.xpath('//input[@id="createKeyData"]')).sendKeys(name))
      .then(() => browser.findElement(by.xpath('//button[text()="Create"]')).click())
  }

  function checkDeviceExists (name) {
    return browser.wait(until.elementLocated(by.xpath(`//th/a[text()="${name}"]`)), TEST_TIMEOUT)
  }

  function checkVpn (done) {
    const client = net.connect({ host: app.fqdn, port: 7494 }, () => {
      client.end()
      done()
    })
    client.on('error', done)
  }

  xit('build app', function () {
    execSync('cloudron build', {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('install app', function () {
    execSync('cloudron install --new --wait --location ' + LOCATION, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })

  it('can get app information', getAppInfo)
  it('can login', login)
  it('can create device', () => createDevice('testdevice'))
  it('device exists', () => checkDeviceExists('testdevice'))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('can restart app', function () {
    execSync('cloudron restart --wait --app ' + app.id)
  })

  it('can login', autoLogin)
  it('device exists', () => checkDeviceExists('testdevice'))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('backup app', function () {
    execSync('cloudron backup create --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('restore app', function () {
    execSync('cloudron restore --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('can login', autoLogin)
  it('device exists', () => checkDeviceExists('testdevice'))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('move to different location', function () {
    execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })
  it('can get new app information', getAppInfo)

  it('can login', autoLogin)
  it('device exists', () => checkDeviceExists('testdevice'))
  it('check vpn', checkVpn)
  it('can logout', logout)

  it('uninstall app', function () {
    execSync('cloudron uninstall --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  // test update (this test will only work after app is published)
  xit('can install app', function () {
    execSync('cloudron install --new --wait --appstore-id io.cloudron.openvpn --location ' + LOCATION, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })

  xit('can get app information', getAppInfo)
  xit('can login', login)

  xit('can update', function () {
    execSync('cloudron install --wait --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  xit('device exists', () => checkDeviceExists('testdevice'))
  xit('check vpn', checkVpn)

  xit('uninstall app', function () {
    execSync('cloudron uninstall --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })
})
