/* global superagent, $, Vue */

(function () {
  'use strict'

  const padLeft = (nr, n, str = '0') => new Array(n - String(nr).length + 1).join(str) + nr

  const cleanDeviceName = deviceName => deviceName.replace(/[^A-Za-z0-9\-_]+/g, '')

  window.app = new Vue({
    el: '#app',
    data: {
      busy: true,
      session: false,
      entries: [],
      isAdmin: false,
      connectedClients: {},
      createKeyData: '',
      createKeyError: null,
      revokeKeyData: '',
      revokeKeyError: null,
      revokeKeyVerify: '',
      revokeKeyVerifyError: null
    },
    computed: {
      keyNameValid () {
        return this.createKeyData === cleanDeviceName(this.createKeyData)
      },
      listConnectedClients () {
        return Object.keys(this.connectedClients)
          .map(user =>
            Object.keys(this.connectedClients[user])
              .map(device => this.connectedClients[user][device]
                ? {
                  user,
                  device,
                  ip: this.connectedClients[user][device].ip,
                  vpnIp: this.connectedClients[user][device].vpnIp
                }
                : null
              )
              .filter(x => x)
          )
          .reduce((a, b) => [].concat(a, b), [])
          .filter(x => x)
      },
      connectedClientsStr () {
        return this.listConnectedClients
          .map(client => `${client.user}&nbsp;:&nbsp;${client.device}&nbsp;:&nbsp;${client.vpnIp}&nbsp;:&nbsp;${client.ip}`)
          .join('<br>')
      },
      nbClients () {
        return this.listConnectedClients.length
      }
    },
    created () {
      this.$nextTick(() => {
        $('#modalCreateKey').on('shown.bs.modal', () => $('#createKeyData').focus())
        $('#modalRevokeKey').on('shown.bs.modal', () => $('#revokeKeyVerify').focus())
      })

      this.refresh()
    },
    methods: {
      logout () {
        window.location.href = '/logout'
      },
      login () {
        window.location.href = '/login'
      },
      welcome () {
        this.session = false
      },
      refresh () {
        this.busy = true

        superagent.get('/api/list/')
          .end((error, result) => {
            this.busy = false

            if (result && result.statusCode === 401) return this.welcome()
            if (error) return console.error(error)

            this.session = true
            this.entries = result.body.entries
            if (result.body.admin && result.body.admin.connectedClients) {
              this.isAdmin = true
              this.connectedClients = result.body.admin.connectedClients
            }

            const $body = $('body')
            $body.on('mouseenter', '[data-toggle=tooltip]', function () {
              const el = $(this)
              if (el.data('tooltip') === undefined) { // init
                el.tooltip({html: true})
              } else { // refresh
                el.tooltip('fixTitle')
              }
              el.tooltip('show')
            })

            $body.on('mouseleave', '[data-toggle=tooltip]', function () {
              $(this).tooltip('hide')
            })
          })
      },
      createKeyAsk () {
        $('#modalCreateKey').modal('show')
        this.createKeyData = ''
        this.createKeyError = null
      },
      revokeKeyAsk (name) {
        $('#modalRevokeKey').modal('show')
        this.revokeKeyData = name
        this.revokeKeyVerify = ''
        this.revokeKeyError = null
        this.revokeKeyVerifyError = ''
      },
      createKey (name) {
        if (!this.keyNameValid) return
        this.busy = true
        const onError = err => {
          console.error('Error creating device: ', err)
          this.createKeyError = 'Error creating device: ' + err
        }

        superagent.put('/api/key/' + name)
          .end((error, result) => {
            this.busy = false

            if (result && result.statusCode === 401) return this.welcome()
            if (result && result.statusCode === 409) {
              this.createKeyError = 'Invalid device name'
              return
            }
            if (result && result.statusCode !== 201) return onError(result.statusCode)
            if (error) return onError(error)

            this.createKeyData = ''
            this.refresh()

            $('#modalCreateKey').modal('hide')
          })
      },
      revokeKey (name, verify) { // TODO: validate name
        this.revokeKeyError = null
        if (name !== verify) {
          this.revokeKeyVerifyError = 'Name does not match'
          return
        }
        this.busy = true
        const onError = err => {
          console.error('Error revoking device: ', err)
          this.revokeKeyError = 'Error revoking device: ' + err
        }

        superagent.delete('/api/key/' + name)
          .end((error, result) => {
            this.busy = false

            if (result && result.statusCode === 401) return this.welcome()
            if (result && result.statusCode === 409) {
              this.revokeKeyError = 'Invalid device name'
              return
            }
            if (result && result.statusCode !== 200) return onError(result.statusCode)
            if (error) return onError(error)

            this.revokeKeyData = ''
            this.revokeKeyVerify = ''
            this.refresh()

            $('#modalRevokeKey').modal('hide')
          })
      },
      prettyDate: value => {
        const d = new Date(value)
        return d.toDateString()
      },
      prettyFullDate: value => {
        const d = new Date(value)
        return d.getFullYear() + '/' + padLeft(d.getMonth(), 2) + '/' + padLeft(d.getDate(), 2) +
          ' ' + d.toTimeString()
      }
    }
  })
})()
